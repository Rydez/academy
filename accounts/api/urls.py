from django.conf.urls import url
from django.contrib import admin

from rest_framework_jwt.views import obtain_jwt_token

from .views import UserCreateAPIView

app_name = 'accounts-api'
urlpatterns = [
  # url(r'^register/$', UserCreateAPIView.as_view(), name='register'),
  # url(r'^login/$', obtain_jwt_token, name='login')
]
