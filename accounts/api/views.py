from django.contrib.auth import get_user_model

from rest_framework.generics import CreateAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST, HTTP_201_CREATED
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.permissions import AllowAny
from rest_framework.authentication import SessionAuthentication, BasicAuthentication

from .serializers import (
  UserCreateSerializer,
  UserLoginSerializer
)

class UserCreateAPIView(CreateAPIView):
  permission_classes = [AllowAny]
  serializer_class = UserCreateSerializer

  def post(self, request, *args, **kwargs):
    serializer = UserCreateSerializer(data=request.data)
    if serializer.is_valid(raise_exception=True):
      serializer.save()
      return Response(serializer.data, status=HTTP_201_CREATED)
    return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)

# class UserLoginAPIView(APIView):
#   permission_classes = [AllowAny]
#   serializer_class = UserLoginSerializer

#   def post(self, request, *args, **kwargs):
#     serializer = UserLoginSerializer(data=request.data)
#     if serializer.is_valid(raise_exception=True):
#       return Response(serializer.data, status=HTTP_200_OK)
#     return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
