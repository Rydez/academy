from importlib import import_module
from django.conf.urls import include, url
from allauth.socialaccount import providers
from .views import RegisterView, LoginView
from django.contrib.auth.views import LogoutView

urlpatterns = [
  url(r'^social/', include('allauth.socialaccount.urls')),
  url(r'^register/', RegisterView.as_view(), name='register'),
  url(r'^login/', LoginView.as_view(), name='login'),
  url(r'^logout/', LogoutView.as_view(), name='logout')
]

for provider in providers.registry.get_list():
  try:
    prov_mod = import_module(provider.get_package() + '.urls')
  except ImportError:
    continue
  prov_urlpatterns = getattr(prov_mod, 'urlpatterns', None)
  if prov_urlpatterns:
    urlpatterns += prov_urlpatterns