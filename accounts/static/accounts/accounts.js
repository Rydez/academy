$(function() {
  // function get_cookie(name) {
  //   let cookie_value = null;
  //   if (document.cookie && document.cookie != '') {
  //     const cookies = document.cookie.split(';');
  //     for (let cookie of cookies) {
  //       cookie = jQuery.trim(cookie);

  //       // Does this cookie string begin with the name we want?
  //       if (cookie.substring(0, name.length + 1) == (name + '=')) {
  //         cookie_value = decodeURIComponent(cookie.substring(name.length + 1));
  //         break;
  //       }
  //     }
  //   }
  //   return cookie_value;
  // }

  // const csrftoken = get_cookie('csrftoken');

  // function csrfSafeMethod(method) {

  //     // these HTTP methods do not require CSRF protection
  //     return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
  // }

  // $.ajaxSetup({
  //   beforeSend: function(xhr, settings) {
  //     if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
  //       xhr.setRequestHeader("X-CSRFToken", csrftoken);
  //     }
  //   }
  // });

  // on blur check for errors or help text to display look in materialize forms.js

  $('#register_form').submit(function() {
    const form_data = $("#register_form").serialize();
    console.log(form_data);
    $.ajax({
      type: 'POST',
      url: '/accounts/register/',
      data: form_data,
      // cache: false,
      // contentType: 'application/json',
      // dataType: 'json',
      success: function() {
        document.location.href = '/';
      },
      error: function(response) {
        const [form] = $('#register_form');
        const form_elements = form.elements;
        for (const element of form_elements) {
          element.classList.remove('invalid');
          element.classList.remove('server-error');
        }

        const field_errors = JSON.parse(response.responseText);
        for (const field in field_errors) {
          const [input] = $('#id_' + field);
          const [label] = input.labels;
          input.classList.add('invalid');
          input.classList.add('server-error');
          let error_message = '';
          for (const error of field_errors[field]) {
            if (error_message === '') {
              error_message = error.message;
            }
            else {
              error_message += (' ' + error.message);
            }
          }

          label.setAttribute('data-error', error_message);
        }
      }
    });

    return false;
  });

  $('#login_form').submit(function() {
    const form_data = $("#login_form").serialize();
    console.log(form_data);
    $.ajax({
      type: 'POST',
      url: '/accounts/login/',
      data: form_data,
      // cache: false,
      // contentType: 'application/json',
      // dataType: 'json',
      success: function() {
        document.location.href = '/';
      },
      error: function(response) {
        const [form] = $('#login_form');
        const form_elements = form.elements;
        for (const element of form_elements) {
          element.classList.add('invalid');
          element.classList.add('server-error');
        }

        const form_error = response.responseText;
        const [error_label] = $('#login_form_error');
        error_label.innerText = form_error;
      }
    });

    return false;
  });
});