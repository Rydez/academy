from django.core.validators import validate_email
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

class RegistrationForm(UserCreationForm):
  email = forms.EmailField(label='Email', required=True)

  class Meta:
    model = User
    fields = ('username', 'email', 'password1', 'password2')

  def __init__(self, *args, **kwargs):
    super(RegistrationForm, self).__init__(*args, **kwargs)
    self.fields['username'].widget.attrs.pop("autofocus", None)
    self.fields['password1'].help_text = '''
      <b>Password Strength:</b><br>
      <span>
        Must contain 8 characters, both letters and numbers.
        Must not be similar to email or username.
        Must not be commonly used password.
      </span>
    '''

    self.fields['email'].help_text = '''
      We will use your email address for things like keeping
      your account secure, helping people find you, and
      sending notifications. You will always have control
      over this functionality through your Account Settings.
    '''

  def save(self, commit=True):
    user = super(RegistrationForm, self).save(commit=False)
    email = self.cleaned_data['email']
    if commit:
      user.save()
    return user


class LoginForm(AuthenticationForm):

  def __init__(self, *args, **kwargs):
    super(LoginForm, self).__init__(*args, **kwargs)
    self.fields['username'].widget.attrs.pop("autofocus", None)