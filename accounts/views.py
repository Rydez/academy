from django.contrib.auth.models import User
from django.contrib.auth import views as auth_views
from django.contrib.auth import login

from django.views import generic
from django.http import HttpResponse, HttpResponseRedirect

from accounts.forms import RegistrationForm

# Create your views here.
class RegisterView(generic.CreateView):
  form_class = RegistrationForm
  model = User

  def post(self, request, *args, **kwargs):
    form = RegistrationForm(request.POST)
    if form.is_valid():
      form.save()
      return HttpResponse(status=201)
    return HttpResponse(form.errors.as_json(), status=400)

class LoginView(auth_views.LoginView):
  def form_invalid(self, form):
    error_message = 'Username or Password invalid. Please try again'
    return HttpResponse(error_message, status=401)

  def form_valid(self, form):
    login(self.request, form.get_user())
    return HttpResponse(status=200)


