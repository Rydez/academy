$(document).ready(function(){
  $('.modal').modal({
    inDuration: 200, // Transition in duration
    outDuration: 150, // Transition out duration
  });

  // Initialize collapse button
  $(".button-collapse").sideNav();
});
