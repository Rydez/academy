from django.views import generic

from .models import Course, Chapter, Video

class IndexView(generic.ListView):
	template_name = 'videos/index.html'
	context_object_name = 'courses'

	def get_queryset(self):
		return Course.objects.all()

class ListView(generic.ListView):
	template_name = 'videos/list.html'
	context_object_name = 'chapters'

	def get_queryset(self):
		return Chapter.objects.filter(course__slug=self.kwargs['course'])

class DetailView(generic.DetailView):
	model = Video
	template_name = 'videos/detail.html'