from django.db import models
from django.utils import timezone

class Course(models.Model):
  name = models.CharField(max_length=200)
  slug = models.CharField(max_length=200, unique=True)

  def __str__(self):
    return self.name

class Chapter(models.Model):
  course = models.ForeignKey(Course, on_delete=models.PROTECT)
  name = models.CharField(max_length=200)
  slug = models.CharField(max_length=200, unique=True)

  def __str__(self):
    return self.name

  def get_videos(self):
    return self.video_set.filter(chapter_id=self.id)

class Video(models.Model):
  chapter = models.ForeignKey(Chapter, on_delete=models.PROTECT)
  name = models.CharField(max_length=200)
  url = models.URLField()
  pub_date = models.DateTimeField('date published')

  def __str__(self):
    return self.name

  def get_vimeo_id(self):
    return self.url.rsplit('/', 1)[-1]