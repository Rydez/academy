$(function() {

  const videos = $('.video-card');
  for (const video of videos) {
    $.ajax({
      type:'GET',
      url: 'http://vimeo.com/api/v2/video/' + video.id + '.json',
      jsonp: 'callback',
      dataType: 'jsonp',
      success: function([data]) {
        const [thumbnail] = video.getElementsByTagName('img');
        thumbnail.src = data.thumbnail_medium;

        const [title] = video.getElementsByTagName('span');
        title.innerText = data.title;

        const [description] = video.getElementsByTagName('p');
        description.innerText = data.description;

        $(video.getAttribute('href')).modal({
          ready: function() {
            var options = {
              width: $(window).width()
            };

            const content = $(this)[0].$el[0].getElementsByClassName('modal-content')[0];
            new Vimeo.Player(content, options);
          }
        });
      }
    });
  }

});