from rest_framework.serializers import ModelSerializer

from ..models import Video

class VideoDetailSerializer(ModelSerializer):
  class Meta:
    model = Video
    fields = [
      'id',
      'chapter',
      'name',
      'url',
      'pub_date'
    ]

class VideoListSerializer(ModelSerializer):
  class Meta:
    model = Video
    fields = [
      'chapter',
      'name',
      'url',
      'pub_date'
    ]