from rest_framework.generics import (
  # DestroyAPIView,
  ListAPIView,
  RetrieveAPIView,
  # UpdateAPIView,
)

from rest_framework.permissions import (
  AllowAny,
  IsAuthenticated,
  IsAdminUser,
  IsAuthenticatedOrReadOnly,
)

from videos.models import Video
from .serializers import VideoDetailSerializer, VideoListSerializer

# class VideoDestroyAPIView(DestroyAPIView):
#   queryset = Video.objects.all()
#   serializer_class = VideoDetailSerializer

class VideoDetailAPIView(RetrieveAPIView):
  queryset = Video.objects.all()
  serializer_class = VideoDetailSerializer
  permission_classes = [IsAuthenticated]

class VideoListAPIView(ListAPIView):
  queryset = Video.objects.all()
  serializer_class = VideoListSerializer
  permission_classes = [IsAuthenticated]

  #def get_queryset():

# class VideoUpdateAPIView(UpdateAPIView):
#   queryset = Video.objects.all()
#   serializer_class = VideoDetailSerializer