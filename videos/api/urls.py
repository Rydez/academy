from django.conf.urls import url
from django.contrib import admin

from .views import (
  VideoListAPIView,
  VideoDetailAPIView,
  # VideoDestroyAPIView,
  # VideoUpdateAPIView,
  )

urlpatterns = [
  url(r'^$', VideoListAPIView.as_view(), name='list'),
  url(r'^(?P<pk>\d+)/$', VideoDetailAPIView.as_view(), name='detail'),
  # url(r'^(?P<pk>\d+)/destroy/$', VideoDestroyAPIView.as_view(), name='destroy'),
  # url(r'^(?P<pk>\d+)/edit/$', VideoUpdateAPIView.as_view(), name='update'),
]

#### SLUG IMPLEMENTATION: https://www.youtube.com/watch?v=dWZB_F32BDg&list=PLEsfXFp6DpzTOcOVdZF-th7BS_GYGguAS&index=6