from django.contrib import admin

from .models import Course, Chapter, Video

admin.site.register(Course)
admin.site.register(Chapter)
admin.site.register(Video)
