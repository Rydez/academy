# from accounts.api.serializers import (
#     UserCreateSerializer,
#     UserLoginSerializer
# )

# from accounts.forms import (
#     LoginForm,
#     # RegisterForm
# )

from accounts.forms import RegistrationForm, LoginForm

def login_form(request):
    # next = request.GET.get('next', '/')
    # login_serializer = UserLoginSerializer()
    # return {'login_serializer': login_serializer}
    # login_form = LoginForm()
    # return {'login_form': login_form}
    login_form = LoginForm()
    return {'login_form': login_form}

def register_form(request):
    # next = request.GET.get('next', '/')
    # register_serializer = UserCreateSerializer()
    # return {'register_serializer': register_serializer}
    register_form = RegistrationForm()
    return {'register_form': register_form}
